"""
Woggle - bot framework

  Copyright (C) 2021 Christos Triantafyllidis <christos.triantafyllidis@gmail.com>
  Copyright (C) 2021 Yorick Bosman <spam@gewoonyorick.nl>

This file is part of Woggle. Woggle is free software: you can redistribute
it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, version 3.

Woggle is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

# Usermodes
def halfvoice(bot, nick, channel, enabled=True):
    """
    Changes a users half-voice (V) flag.
    """
    if enabled:
        bot.write(["MODE", channel, "+V", nick])
    else:
        bot.write(["MODE", channel, "-V", nick])


def voice(bot, nick, channel, enabled=True):
    """
    Changes a users voice (v) flag.
    """
    if enabled:
        bot.write(["MODE", channel, "+v", nick])
    else:
        bot.write(["MODE", channel, "-v", nick])


def halfop(bot, nick, channel, enabled=True):
    """
    Changes a users half-op (h) flag.
    """
    if enabled:
        bot.write(["MODE", channel, "+h", nick])
    else:
        bot.write(["MODE", channel, "-h", nick])


def op(bot, nick, channel, enabled=True):  # pylint: disable=invalid-name
    """
    Changes a users op (o) flag.
    """
    if enabled:
        bot.write(["MODE", channel, "+o", nick])
    else:
        bot.write(["MODE", channel, "-o", nick])


def admin(bot, nick, channel, enabled=True):
    """
    Changes a users admin (a) flag.
    """
    if enabled:
        bot.write(["MODE", channel, "+a", nick])
    else:
        bot.write(["MODE", channel, "-a", nick])


def owner(bot, nick, channel, enabled=True):
    """
    Changes a users op (q) flag.
    """
    if enabled:
        bot.write(["MODE", channel, "+q", nick])
    else:
        bot.write(["MODE", channel, "-q", nick])


def oper(bot, nick, channel, enabled=True):
    """
    Changes a users oper (Y) flag.
    """
    if enabled:
        bot.write(["MODE", channel, "+Y", nick])
    else:
        bot.write(["MODE", channel, "-Y", nick])


# Channel Modes
def moderated(bot, channel, enabled=True):
    """
    Changes the channel's moderation (m) flag.
    """
    if enabled:
        bot.write(["MODE", channel, "+m"])
    else:
        bot.write(["MODE", channel, "-m"])


def blocknickchanges(bot, channel, enabled=True):
    """
    Changes the channel's nickchange (N) flag.
    """
    if enabled:
        bot.write(["MODE", channel, "+N"])
    else:
        bot.write(["MODE", channel, "-N"])
