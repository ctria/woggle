"""
Woggle - bot framework

  Copyright (C) 2021 Christos Triantafyllidis <christos.triantafyllidis@gmail.com>
  Copyright (C) 2021 Yorick Bosman <spam@gewoonyorick.nl>

This file is part of Woggle. Woggle is free software: you can redistribute
it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, version 3.

Woggle is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import time

import sopel.module as module

from helpers import auth, timer
from helpers.channelmanagement import blocknickchanges, moderated, voice
from helpers.sa import sajoin, sanick, sapart


class MurderMystery:
    """
    Config:

    [murdermystery]
    channel_game = #murder-mystery
    channel_management = #murder-mystery-control
    channel_death = #murder-mystery-deathchat
    deathchat_enabled = True

    [permissions]
    murdermystery = "admin1, admin2"
    """

    def __init__(self, bot):
        self.players = {}
        self.roles = {}
        self.game_status = "SETUP"
        self.game_channel = bot.config.murdermystery.channel_game
        self.management_channel = bot.config.murdermystery.channel_management
        self.deathchat_channel = bot.config.murdermystery.channel_death
        self.deathchat_enabled = bot.config.murdermystery.deathchat_enabled
        self.messages = {
            "intro": [],
            "epilogue": [],
            "role_prefix": [],
            "role_suffix": [],
        }

    def status_get(self, prettified=False):
        status = self.game_status

        prettify_map = {
            "SETUP": "\x0308Setting up\x0F",
            "PENDING": "\x0307Pending\x0F",
            "STARTED": "\x0303Started\x0F",
            "STOPPED": "\x0304Stopped\x0F",
        }

        if prettified is False:
            return status
        return prettify_map[status]

    def player_get_role(self, player):
        if player in self.players:
            return self.players[player]["role"]
        return False

    def player_get_nick(self, role):
        for user in self.players:
            if self.players[user]["role"] == role:
                return user
        return None

    def player_get_role_nick(self, player):
        if player in self.players:
            role = self.player_get_role(player)
            return {"nick": player, "role": role}

    def get_text_arg(self, args, role_remove=True):
        cmd = args.group(3)
        role = args.group(4)
        text = args.group(2)
        if role_remove:
            return text.replace(f"{cmd} {role} ", "", 1)
        return text.replace(f"{cmd} ", "", 1)

    #
    #   Intro & Epilogue messages
    #
    #   Intro mesages get displayed at the start of the game, epilogue when the game ends.
    #

    def text_send(self, bot, kind, to=None):  # pylint: disable=invalid-name
        """ Send intro, epilogue, role_prefix or role_suffix to the game channel or otherwise supplied receiver """
        if to is None:
            to = self.game_channel
        for message in self.messages[kind]:
            bot.say(message, to)

    # Kill timer

    @timer.resetable_timer(600)
    def kill_timer(self, bot):
        bot.say(
            "It has been 10 minutes since the last kill. It is suggested to kill someone again now.",
            self.management_channel,
        )

    # Commands

    def command_text_add(self, bot, trigger, kind):
        text = self.get_text_arg(trigger, role_remove=False)
        self.messages[kind].append(text)
        bot.say(
            f"The text has been added succesfully to the {kind}, to view all messages use the "
            f".mm show{kind} command."
        )

    def command_text_clear(self, bot, trigger, kind):  # pylint: disable=unused-argument
        self.messages[kind] = []
        bot.say(f"Succesfully cleared the {kind} messages!")

    def command_text_show(self, bot, trigger, kind):  # pylint: disable=unused-argument
        messages = self.messages[kind]
        if messages == []:
            bot.say(f"No messages are configured for {kind}!")
        else:
            num = len(messages)
            bot.say(f"{num} messages are configured for the {kind}:")
            for message in messages:
                bot.say(message)
            bot.say(f"End of messages for the {kind}!")

    def commandAddRole(self, bot, trigger):
        role = trigger.group(4).lower()
        if role in self.roles:
            bot.say(f"The role {role} already excists!")
        else:
            self.roles[role] = {"start_messages": []}
            bot.say(f"The role {role} has been created succesfully!")

    def commandRemoveRole(self, bot, trigger):
        role = trigger.group(4).lower()
        if role in self.roles:
            self.roles.pop(role)
            bot.say(f"The role {role} has been removed succesfully!")
        else:
            bot.say(f"The role {role} doesn't excist!")

    def commandAddMessages(self, bot, trigger):
        text = self.get_text_arg(trigger)
        role = trigger.group(4).lower()
        if role in self.roles:
            self.roles[role]["start_messages"].append(text)
            bot.say(
                f"The text has been added succesfully to the role {role}, to view all messages use the .mm showmessages <role> command."
            )
        else:
            bot.say(f"The role {role} doesn't excist!")

    def commandShowMessages(self, bot, trigger):
        role = trigger.group(4).lower()
        if role in self.roles:
            messages = self.roles[role]["start_messages"]
            if messages == []:
                bot.say(f"No messages are configured for the role {role}!")
            else:
                num = len(messages)
                bot.say(f"{num} messages are configured for the role {role}:")
                for message in messages:
                    bot.say(message)
                bot.say(f"End of messages for role {role}!")
        else:
            bot.say(f"The role {role} doesn't excist!")

    def commandClearMessages(self, bot, trigger):
        role = trigger.group(4).lower()
        if role in self.roles:
            self.roles[role]["start_messages"] = []
            bot.say(f"Succesfully cleared messages for the role {role}!")
        else:
            bot.say(f"The role {role} does not excist!")

    def commandAssignRole(self, bot, trigger):
        player = trigger.group(4)
        role = trigger.group(5).lower()

        # Check if player has a role
        if player in self.players:
            player_role = self.player_get_role(player)
            bot.say(
                f"{player} already has the role {player_role}! Remove this role before assigning a new one."
            )

        else:
            # Check if role excists
            if role in self.roles:
                self.players[player] = {"role": role, "nickname": player, "dead": False}
                bot.say(f"The role {role} has succesfully been assigned to {player}")
                if self.game_status == "STARTED":
                    # Do sanicks & send messages
                    # Send role prefix
                    self.text_send(bot, "role_prefix", to=player)

                    messages = self.roles[role]["start_messages"]
                    for message in messages:
                        bot.say(message, player)

                    # Send role suffix
                    self.text_send(bot, "role_prefix", to=player)

                    voice(bot, player, self.game_channel)

                    # wait for the stuff to get send
                    time.sleep(1)
                    sanick(bot, player, role)
            else:
                bot.say(f"The role {role} does not excist!")

    def commandKillPlayer(self, bot, trigger):
        player = trigger.group(4)
        player_data = self.player_get_role_nick(player)
        if player_data is not None:
            if self.players[player_data["nick"]]["dead"]:
                return bot.say(
                    "You really want to be sure that they're dead? (Already dead)"
                )
            else:
                self.players[player_data["nick"]]["dead"] = True
                voice(bot, player_data["role"], self.game_channel, False)

                # Reset kill game timer
                self.kill_timer(bot).reset(start=True)

                if self.deathchat_enabled:
                    sajoin(bot, player_data["role"], self.deathchat_channel)
                time.sleep(1)
                sanick(bot, player_data["role"], player_data["role"] + "_dead")
        else:
            bot.say(f"{player} could not be found. Please try again.")

    def commandRevokeRole(self, bot, trigger):
        player = trigger.group(4)

        # Check if player has a role
        if player in self.players:
            role = self.player_get_role(player)
            self.players.pop(player)
            bot.say(
                f"The player {player} has succesfully been removed from the role {role}"
            )
        else:
            bot.say(f"{player} doesn't have a role yet.")

    def commandStartGame(self, bot, trigger):
        currentstatus = self.status_get()
        if currentstatus != "STARTED":
            # Add check if all assigned role people are actually there

            # Do sanicks & send messages
            players = self.players
            for playerid in players:
                player = self.players[playerid]

                # Send role prefix
                self.text_send(bot, "role_prefix", to=player["nickname"])

                messages = self.roles[player["role"]]["start_messages"]
                for message in messages:
                    bot.say(message, player["nickname"])

                # Send role suffix
                self.text_send(bot, "role_prefix", to=player["nickname"])

                voice(bot, player["nickname"], self.game_channel)

            # wait for the stuff to get send
            time.sleep(1)
            for playerid in players:
                player = self.players[playerid]
                sanick(bot, player["nickname"], player["role"])

            # Set modes
            moderated(bot, self.game_channel)
            blocknickchanges(bot, self.game_channel)

            # Send intro
            self.text_send(bot, "intro")

            # Start game timer
            self.kill_timer(bot).start()

            # Change status
            self.game_status = "STARTED"
            bot.say(
                "The game has succesfully been started! See your pm for your role explanation!",
                self.game_channel,
            )
        else:
            bot.say("The game has already started.")

    def commandStopGame(self, bot, trigger):  # pylint: disable=unused-argument
        currentstatus = self.status_get()
        if currentstatus == "STARTED":
            # Add check if all assigned role people are actually there

            # Do sanicks
            players = self.players
            for playerid in players:
                player = self.players[playerid]
                is_dead = self.players[player["nickname"]]["dead"]
                currentnick = player["role"]
                if is_dead:
                    currentnick = currentnick + "_dead"
                voice(bot, currentnick, self.game_channel, False)
                if self.deathchat_enabled:
                    if self.players[player["nickname"]]["dead"]:
                        sapart(
                            bot,
                            currentnick,
                            self.deathchat_channel,
                            "Murdermystery game ended.",
                        )

            # wait for the stuff to get send
            time.sleep(1)
            for playerid in players:
                player = self.players[playerid]
                currentnick = player["role"]
                is_dead = self.players[player["nickname"]]["dead"]
                if is_dead:
                    currentnick = currentnick + "_dead"
                sanick(bot, currentnick, player["nickname"])

            # Set modes
            moderated(bot, self.game_channel, False)
            blocknickchanges(bot, self.game_channel, False)

            # Send epilogue
            self.text_send(bot, "epilogue")

            # Stop game timer
            self.kill_timer(bot).cancel()

            # Change status
            self.game_status = "STOPPED"
            bot.say("The game has succesfully been stopped!", self.game_channel)
        else:
            bot.say("The game has to be started in order for it to stop")

    def commandClearGame(self, bot, trigger):  # pylint: disable=unused-argument
        self.players = {}
        self.roles = {}
        self.game_status = "SETUP"
        bot.say("Cleared users & roles")

    def commandClearUsers(self, bot, trigger):  # pylint: disable=unused-argument
        for player in dict(self.players):
            self.players.pop(player)
        bot.say("Cleared users")

    def commandStatus(self, bot, trigger):  # pylint: disable=unused-argument
        status = self.status_get(prettified=True)
        bot.say(f"Game status is: {status}")
        status = self.status_get()
        if status == "SETUP":
            return  # TODO: implement status overview pre-game
        if status == "STARTED":
            return  # TODO: implement status overview when the game has started
        if status == "STOPPED":
            alive = []
            dead = []
            for playerid in self.players:
                player = self.players[playerid]
                if player["dead"]:
                    dead.append(player["nickname"])
                else:
                    alive.append(player["nickname"])
            alive_players = fancy_list_maker(alive)
            dead_players = fancy_list_maker(dead)
            bot.say(f"Players still alive: {alive_players}")
            bot.say(f"Players who died: {dead_players}")
        bot.say("End of status")

    def commandInfo(self, bot, trigger):
        player_req = trigger.group(4)
        player = self.player_get_role_nick(player_req)
        if player is None:
            bot.say("Player or role not found. Please try again.")
        else:
            role = player["role"]
            nick = player["nick"]
            bot.say(
                f"Player has the role \x02{role}\x0F and has the nickname \x02{nick}\x0F"
            )


murdermystery = None


def setup(bot):
    global murdermystery
    murdermystery = MurderMystery(bot)


@module.commands("mm", "murdermystery")
@auth.require_permission("murdermystery")
def command(bot, trigger):
    if trigger.group(3) is None:
        return bot.say(
            "Please provide a subcommand. See the documentation for more information."
        )

    command_map = {
        "createrole": murdermystery.commandAddRole,
        "removerole": murdermystery.commandRemoveRole,
        "addmessage": murdermystery.commandAddMessages,
        "clearmessages": murdermystery.commandClearMessages,
        "showmessages": murdermystery.commandShowMessages,
        "assign": murdermystery.commandAssignRole,
        "resign": murdermystery.commandRevokeRole,
        "kill": murdermystery.commandKillPlayer,
        "start": murdermystery.commandStartGame,
        "stop": murdermystery.commandStopGame,
        "cleargame": murdermystery.commandClearGame,
        "clearusers": murdermystery.commandClearUsers,
        "status": murdermystery.commandStatus,
        "info": murdermystery.commandInfo,
        "showintro": [murdermystery.command_text_show, "intro"],
        "showepilogue": [murdermystery.command_text_show, "epilogue"],
        "showroleprefix": [murdermystery.command_text_show, "role_prefix"],
        "showrolesuffix": [murdermystery.command_text_show, "role_suffix"],
        "addintro": [murdermystery.command_text_add, "intro"],
        "addepilogue": [murdermystery.command_text_add, "epilogue"],
        "addroleprefix": [murdermystery.command_text_add, "role_prefix"],
        "addrolesuffix": [murdermystery.command_text_add, "role_suffix"],
        "clearintro": [murdermystery.command_text_clear, "intro"],
        "clearepilogue": [murdermystery.command_text_clear, "epilogue"],
        "clearroleprefix": [murdermystery.command_text_clear, "role_prefix"],
        "clearrolesuffix": [murdermystery.command_text_clear, "role_suffix"],
    }

    try:
        if isinstance(command_map[trigger.group(3)], list):
            command_map[trigger.group(3)][0](
                bot, trigger, command_map[trigger.group(3)][1]
            )
        else:
            command_map[trigger.group(3)](bot, trigger)
    except KeyError:
        bot.say(
            "Sorry, I didn't quite catch that. Please check the documentation to make sure the command you want to use is available."
        )


def fancy_list_maker(names):
    string = ", ".join(names)
    return " and ".join(string.rsplit(", ", 1))
