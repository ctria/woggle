default: clean install

clean:
	find . -name '*.pyc' -exec rm -rf {} +
	find . -name '__pycache__' -exec rm -rf {} +
	find . -name '*.egg-info' -exec rm -rf {} +

install:
	pip3 install -r requirements.txt
	pip3 install -r dev-requirements.txt

lint:
	black --check --diff modules tools helpers
	isort --check-only --diff modules tools helpers

format:
	black modules tools helpers
	isort modules tools helpers

copyright:
	find . -name *.py -not -size 0 |xargs ./tools/copyright.py

start:
	PYTHONPATH=. sopel start -c woggle-dev.cfg -d

restart:
	PYTHONPATH=. sopel restart -c woggle-dev.cfg

stop:
	PYTHONPATH=. sopel stop -c woggle-dev.cfg

run: 
	PYTHONPATH=. sopel start -c woggle-dev.cfg
