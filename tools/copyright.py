#!/usr/bin/env python3
"""
Woggle - bot framework

  Copyright (C) 2021 Christos Triantafyllidis <christos.triantafyllidis@gmail.com>
  Copyright (C) 2021 Yorick Bosman <spam@gewoonyorick.nl>

This file is part of Woggle. Woggle is free software: you can redistribute
it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, version 3.

Woggle is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import re
import subprocess
from datetime import datetime
from sys import argv

DATE_FORMAT = "%a %b %d %H:%M:%S %Y %z"

COPYRIGHT_MESSAGE = (
    lambda: f"""
Woggle - bot framework

{authors_msg}
This file is part of Woggle. Woggle is free software: you can redistribute
it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, version 3.

Woggle is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
)

for my_file in argv[1:]:

    # Check if the dir is a virtualenv, and skip checking if it is
    # We assume the virtualenv is stored in woggle or virtualenv
    directory = my_file.split("/")
    if directory[1] == "virtualenv" or directory[1] == "woggle":
        continue

    authors = {}
    result = subprocess.run(
        ["git", "log", "--use-mailmap", my_file], stdout=subprocess.PIPE
    )
    git_log_stdout = result.stdout.decode("utf8")
    for commit in git_log_stdout.split("commit"):
        try:
            author = re.search("Author: (.*)\n", commit)[1].strip()
            year = datetime.strptime(
                re.search("Date: (.*)\n", commit)[1].strip(), DATE_FORMAT
            ).year
            try:
                authors[author].add(year)
            except KeyError:
                authors[author] = set([year])
        except TypeError:
            pass

    authors_msg = ""
    for author in authors:
        years = ",".join([str(year) for year in authors[author]])
        authors_msg += f"  Copyright (C) {years} {author}\n"
    authors_msg = "\n".join(sorted(authors_msg.split("\n")[:-1])) + "\n"
    copyright_message = COPYRIGHT_MESSAGE()

    start_of_file = True
    updated_contents = ""
    in_docstring = False
    done_docstring = False
    with open(my_file) as f:
        for line in f:
            if not (done_docstring or in_docstring) and (
                line.startswith('"""') or line.startswith("'''")
            ):
                in_docstring = True
                continue

            if in_docstring and (
                line.strip().endswith('"""') or line.strip().endswith("'''")
            ):
                in_docstring = False
                continue

            if start_of_file:
                if line.startswith("#!"):
                    updated_contents = line
                updated_contents += '"""'
                updated_contents += copyright_message
                updated_contents += '"""\n'
                start_of_file = False
                continue

            if not in_docstring:
                updated_contents += line
                done_docstring = True

    with open(my_file, "w") as f:
        f.write(updated_contents)
